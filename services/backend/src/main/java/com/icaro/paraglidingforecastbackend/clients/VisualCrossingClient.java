package com.icaro.paraglidingforecastbackend.clients;

import com.icaro.paraglidingforecastbackend.dto.VisualCrossing.VisualCrossingDto;
import feign.Param;
import feign.RequestLine;

public interface VisualCrossingClient {
  @RequestLine(
      "GET /VisualCrossingWebServices/rest/services/timeline/{coordinates}?unitGroup=metric&include=hours")
  VisualCrossingDto getForecast(@Param("coordinates") String coordinates);
}
