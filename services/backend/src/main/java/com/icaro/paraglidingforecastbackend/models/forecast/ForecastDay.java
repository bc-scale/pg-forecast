package com.icaro.paraglidingforecastbackend.models.forecast;

import lombok.Data;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

@Data
public final class ForecastDay {
  private final LocalDate date;
  private Double score;
  private final LocalTime sunrise;
  private final LocalTime sunset;
  private final List<ForecastHour> hours;
}
