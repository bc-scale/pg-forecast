package com.icaro.paraglidingforecastbackend.clients;

import com.icaro.paraglidingforecastbackend.dto.WeatherApi.WeatherApiDto;
import feign.Param;
import feign.RequestLine;

public interface WeatherApiClient {
  @RequestLine("GET forecast.json?q={coordinates}&days=3")
  WeatherApiDto getForecast(@Param("coordinates") String coordinates);
}
