package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiDto {
  private final WeatherApiLocation location;
  private final WeatherApiForecast forecast;

  public WeatherApiDto(
      @JsonProperty("location") WeatherApiLocation location,
      @JsonProperty("forecast") WeatherApiForecast forecast) {
    this.location = location;
    this.forecast = forecast;
  }
}
