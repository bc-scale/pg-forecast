package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.clients.WeatherApiClient;
import com.icaro.paraglidingforecastbackend.mappers.weatherApi.WeatherApiMapper;
import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.location.Location;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherModelServiceWeatherApi implements WeatherModelService {

  private final WeatherApiClient weatherApiClient;
  private final WeatherApiMapper weatherApiMapper;

  public WeatherModelServiceWeatherApi(
      WeatherApiClient weatherApiClient, WeatherApiMapper weatherApiMapper) {
    this.weatherApiClient = weatherApiClient;
    this.weatherApiMapper = weatherApiMapper;
  }

  public List<Forecast> getForecasts(List<Location> locations) {
    List<Forecast> forecasts = new ArrayList<>();
    for (Location location : locations) {
      forecasts.add(
          weatherApiMapper.toForecast(
              new DateTime(),
              weatherApiClient.getForecast(location.getCoordinates().toString()),
              location));
    }

    return forecasts;
  }

  public Forecast getForecast(Location location) {
    return weatherApiMapper.toForecast(
        new DateTime(),
        weatherApiClient.getForecast(location.getCoordinates().toString()),
        location);
  }
}
