package com.icaro.paraglidingforecastbackend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.icaro.paraglidingforecastbackend.clients.WeatherApiClient;
import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Bean;

@ConstructorBinding
@ConfigurationProperties(prefix = "weather-api")
public class WeatherApiClientConfiguration {

    private static final String WEATHERAPI_URL = "https://api.weatherapi.com/v1";

    private final String apiKey;

    public WeatherApiClientConfiguration(String apiKey) {
        this.apiKey = apiKey;
    }

    @Bean
    public WeatherApiClient weatherApiClient() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());

        return Feign.builder()
                .encoder(new JacksonEncoder(objectMapper))
                .decoder(new JacksonDecoder(objectMapper))
                .requestInterceptor(new WeatherApiInterceptor(apiKey))
                .target(WeatherApiClient.class, WEATHERAPI_URL);
    }

    private static class WeatherApiInterceptor implements RequestInterceptor {
        private final String apiKey;

        public WeatherApiInterceptor(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public void apply(RequestTemplate template) {
            final String uri = String.format("%s&key=%s", template.url().split(WEATHERAPI_URL)[0], apiKey);
            template.uri(uri);
        }
    }
}
