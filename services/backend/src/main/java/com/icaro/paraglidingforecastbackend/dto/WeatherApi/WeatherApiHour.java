package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiHour {
  private final LocalTime hour;
  private final double temp;

  private final double windSpeed;
  private final double windGust;
  private final double windDir;

  private final double humidity;
  private final double precipitation;

  private final double visibility;
  private final double cloudCover;

  public WeatherApiHour(
      @JsonProperty("time") final String hour,
      @JsonProperty("temp_c") final Double temp,
      @JsonProperty("humidity") final Double humidity,
      @JsonProperty("precip_mm") final Double precipitation,
      @JsonProperty("wind_kph") final Double windSpeed,
      @JsonProperty("gust_kph") final Double windGust,
      @JsonProperty("wind_degree") final Double windDir,
      @JsonProperty("cloud") final Double cloudCover,
      @JsonProperty("vis_km") final Double visibility) {

    this.hour = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm").parseLocalTime(hour);
    this.temp = temp;
    this.humidity = humidity;
    this.precipitation = precipitation;
    this.windGust = windGust;
    this.windSpeed = windSpeed;
    this.windDir = windDir;
    this.cloudCover = cloudCover;
    this.visibility = visibility;
  }
}
