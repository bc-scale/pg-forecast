package com.icaro.paraglidingforecastbackend.mappers.visualCrossing;

import com.icaro.paraglidingforecastbackend.dto.VisualCrossing.VisualCrossingHour;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastHour;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface VisualCrossingHourMapper {

  @Mapping(target = "cloudCoverLow", source = "cloudCover")
  @Mapping(target = "cloudCoverMedium", source = "cloudCover")
  @Mapping(target = "cloudCoverHigh", source = "cloudCover")
  @Mapping(target = "score", ignore = true)
  ForecastHour toForecastHour(VisualCrossingHour visualCrossingHour);
}
