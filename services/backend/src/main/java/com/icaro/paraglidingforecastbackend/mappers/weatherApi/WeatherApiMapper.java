package com.icaro.paraglidingforecastbackend.mappers.weatherApi;

import com.icaro.paraglidingforecastbackend.dto.WeatherApi.WeatherApiDto;
import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.location.Location;
import com.icaro.paraglidingforecastbackend.services.ScoringService;
import org.joda.time.DateTime;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    uses = {WeatherApiDayMapper.class})
public abstract class WeatherApiMapper {

  @Autowired protected ScoringService scoringService;

  @Mapping(target = "days", source = "weatherApiDto.forecast.days")
  @Mapping(target = "location", source = "location")
  public abstract Forecast toForecast(
      DateTime forecastTimestamp, WeatherApiDto weatherApiDto, Location location);

  @AfterMapping
  protected void afterMapping(@MappingTarget Forecast forecast) {
    scoringService.scoreForecast(forecast);
  }
}
