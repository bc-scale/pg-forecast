package com.icaro.paraglidingforecastbackend.models.forecast;

import lombok.Data;
import org.joda.time.LocalTime;

@Data
public final class ForecastHour {
  private final LocalTime hour;
  private Double score;

  private final Double temp;

  private final Double windSpeed;
  private final Double windGust;
  private final Double windDir;

  private final Double precipitation;
  private final Double humidity;

  private final Double cloudCoverLow;
  private final Double cloudCoverMedium;
  private final Double cloudCoverHigh;
  private final Double visibility;
}
