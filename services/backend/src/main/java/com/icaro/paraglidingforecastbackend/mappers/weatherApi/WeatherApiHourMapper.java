package com.icaro.paraglidingforecastbackend.mappers.weatherApi;

import com.icaro.paraglidingforecastbackend.dto.WeatherApi.WeatherApiHour;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastHour;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface WeatherApiHourMapper {

  @Mapping(target = "cloudCoverLow", source = "cloudCover")
  @Mapping(target = "cloudCoverMedium", source = "cloudCover")
  @Mapping(target = "cloudCoverHigh", source = "cloudCover")
  @Mapping(target = "score", ignore = true)
  ForecastHour toForecastHour(WeatherApiHour weatherApiHour);
}
