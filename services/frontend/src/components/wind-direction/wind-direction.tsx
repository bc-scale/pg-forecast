import arrow from "../../resources/arrow.svg";
import React from "react";
import styles from "./wind-direction.module.css";
import commonStyles from "../common-styles.module.css";

interface IProps {
    windDirection: number
}

const WindDirection: React.FunctionComponent<IProps> = (props) => (
    <div className={commonStyles.label}>{
        <img className={styles.windDirectionArrow}
             style={{transform: `rotate(${props.windDirection + 180}deg)`}}
             src={arrow}
             alt={"Nothing here"}/>}
    </div>
)

export default WindDirection;