import React from "react";
import styles from "./api-error.module.css";

const ApiError: React.FunctionComponent = () => {
    return (
        <div className={styles.errorHolder}>
            Something went wrong with the API!
        </div>
    );
}

export default ApiError;