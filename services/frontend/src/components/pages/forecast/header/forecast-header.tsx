import React from "react";
import styles from "./forecast-header.module.css"
import Dropdown from "react-dropdown";
import "./dropdown.css"

interface IProps {
    models: string[];
    setSelectedModel: Function;
}

const ForecastHeader: React.FunctionComponent<IProps> = (props) => {
    const {
        models,
        setSelectedModel
    } = props;

    return (
        <div className={styles.header}>
            <div className={styles.headerOrganizer}>
                <div className={styles.title}>Current model used:</div>
                <Dropdown
                    options={models} onChange={(el) => setSelectedModel(el.value)
                } value={models[0]}/>
            </div>
        </div>
    );
}

export default ForecastHeader;
