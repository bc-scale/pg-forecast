import styles from "../forecast-page.module.css";
import ForecastHeader from "../header/forecast-header";
import {setSelectedModel} from "../../../../store/reducers/models-forecast";
import ForecastTable from "../forecast-table/forecast-table";
import React from "react";
import {useDispatch} from "react-redux";
import {ModelsForecast} from "../../../../dto/LocationForecast";

interface IProps {
    modelsForecast: ModelsForecast,
    models: string[],
    selectedModel: string,
}

const Forecast: React.FunctionComponent<IProps> = (props) => {
    const {modelsForecast, models, selectedModel} = props;

    const dispatch = useDispatch();

    return (
        <div className={styles.homeHolder}>

            <ForecastHeader models={models}
                            setSelectedModel={(selectedModel: string) => dispatch(setSelectedModel(selectedModel))}/>

            <div className={styles.forecastTable}>
                <ForecastTable locationsForecast={
                    modelsForecast[selectedModel]}/>
            </div>

        </div>
    );
}

export default Forecast;