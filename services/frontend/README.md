# Frontend Service

## Table of Contents

- [Overview](#overview)
- [Stack](#stack)
- [Requirements](#requirements)
- [How to Develop](#how-to-develop)
  * [Build the image](#build-the-image)

## Overview

This service shows to the end user all relevant data to enable him to make the best decision to where to paragliding.

## Stack

* [React](https://reactjs.org)
* [Redux](https://redux.js.org/)
* [Typescript](https://www.typescriptlang.org/)

## Requirements

This service was tested and developed using Node 16 and npm 8.

## How to Develop

Start by installing all dependencies.

```bash
npm install
```

Then you can simply run the following command.

```bash
npm start
```

Any change you make on the code will be reflected as soon as you save. The site will be available at [http://localhost:3000](http://localhost:3000).

## Build the image

First, you'll need to build the image.

```bash
docker build . -t paragliding-frontend-service
```

This will do everything needed for the database to have the locations loaded. Then, you only need to run it.

```bash
docker run -p 3000:3000 --name paragliding-frontend -d paragliding-frontend-service
```
