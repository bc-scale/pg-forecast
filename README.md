# Paragliding Forecast

## Table of Contents

- [Overview](#overview)
    * [Future Goals](#future-goals)
- [Services](#services)
  * [Backend Service](#backend-service)
  * [Frontend Service](#frontend-service)
- [Mocks](#mocks)
  * [Database](#database)
- [How to Run](#how-to-run)
    * [Locally](#locally)
    * [Docker](#docker)
    * [Docker Compose](#docker-compose)

## Overview

This project aims to build a prediction system to know the best spots to fly on a given day.
Currently, using free weather APIs, it stores processed data, which is feed to the frontend.

### Future Goals
* Scrape data from WindGuru
* Change from NoSQL or add SQL database.
  * Permanently store data.
  * Store if a day was flyable.
  * Add caching solution.
* Implement data model to better predict if day is flyable.

## Services

### Backend Service

The backend service is responsible for fetching, processing, storing and delivering forecasts.

For more information, click [here](services/backend/README.md).

### Frontend Service

The frontend service is showing the data processed and stored by the backend is a clear manner.

For more information, click [here](services/frontend/README.md).

## Mocks

### Database

This service is responsible for fetching, processing, storing and delivering forecast data.

For more information, click [here](database/README.md).

## How to Run

You should create files `application-<compose/local/docker>` according to your use case on the backend service. You can copy the application.yml and go from there.

### Locally

* Set the spring active profile to "local".
* To run this project locally, you should have a mongodb running. You can either start a docker container with mongdb, or use mongodb community.
  * If you don't use the database image, add your own locations or add the locations in `/database/resources/locations.json`. 
More information [here](database/README.md#mock-the-database-without-using-this-image).

```bash
docker run -p 27017:27017 --name paragliding-mongo -d mongo:5.0.5
```

Install all dependencies.
```bash
cd services/backend
mvn install
cd ../frontend
npm install
```


Start the backend using your IDE or CLI.
```bash
cd services/backend
mvn spring-boot:run
```

Start the frontend using your IDE or CLI.
```bash
cd services/frontend
npm start
```

The backend will use the port 8080, and the frontend the port 3000.

### Docker

Similar to running the project locally, you should also have a mongodb running.

If you decide to use mongo in a container, you will also have to add a docker network. If not, ignore the network commands/parameters.

```bash
docker network create paragliding-network

docker run -p 27017:27017 --network paragliding-network --name paragliding-mongo -d mongo:5.0.5

docker build . -t paragliding-forecast 
docker run -p 80:80 -p 8080:8080 --network paragliding-network --name paragliding-forecast-services -d paragliding-forecast
```

The backend will use the port 8080, and the frontend the port 80.

### Docker Compose

In this case, everything will be run in docker, and the network will be set up automatically.

Use `docker-compose build` for subsequent runs.

```bash
docker-compose up
```